## Android Web View app

This repository contains Android WebView app (/WebView) that serves as a wrapper around a website. The website address is in a configuration file: /WebViewer/Assets/config.json.
The repository contains a simple AppBuilder GUI (/AppBuilder). This requires to have msbuild and other necessary tools installed for building apps without VisualStudio IDE installed.
