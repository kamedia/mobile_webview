﻿using Newtonsoft.Json.Linq;
using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppBuilder
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        JObject config;

        public JObject Config { get => config; }

        public ConfigWindow(JObject config)
        {
            InitializeComponent();
            this.config = config;
            msbuildTextBox.Text = config.Value<string>((string)msbuildTextBox.Tag);
            appProjectDirectoryTextBox.Text = config.Value<string>((string)appProjectDirectoryTextBox.Tag);

        }

        private void folderTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();

            if (dialog.ShowDialog(this) ?? false)
            {
                config.Property((string)((FrameworkElement)sender).Tag).Value = dialog.SelectedPath + "\\";
                ((TextBox)sender).Text = dialog.SelectedPath;
            }
        }
    }
}
