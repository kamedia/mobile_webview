﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string appDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        string configFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Kalaam\AppBuilder\";

        JObject config;

        public MainWindow()
        {
            InitializeComponent();

            config = LoadConfig();
        }

        private void SaveConfig()
        {
            JObject json = new JObject();
            json.Add(new JProperty("msbuildDirectory", @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\"));
            json.Add(new JProperty("appProjectDirectory", @"C:\Users\roman\Documents\Visual Studio 2019\Projects\AndroidWebView\WebViewer\"));
            json.Add(new JProperty("appProjectFile", @"WebViewer.csproj"));
            json.Add(new JProperty("appConfigFile", @"Assets\config.json"));
            SaveConfig(json);
        }

        private void SaveConfig(JObject config)
        {
            StreamWriter jsonConfigWriter = new StreamWriter(File.Create(configFileDirectory + "config.json"));
            string jsonString = JsonConvert.SerializeObject(config);
            System.Diagnostics.Debug.WriteLine("json: " + jsonString);
            jsonConfigWriter.Write(jsonString);
            jsonConfigWriter.Close();
        }

        private JObject LoadConfig()
        {
            if (!File.Exists(configFileDirectory + "config.json"))
            {
                Directory.CreateDirectory(configFileDirectory);
                SaveConfig();

            }
            return JsonConvert.DeserializeObject<JObject>(File.ReadAllText(configFileDirectory + "config.json"));
        }

        private void buildButton_Click(object sender, RoutedEventArgs e)
        {
            // FIX Status text not updated until end of this method and the whole UI frozen - render thread blocked!
            buildButton.IsEnabled = false;

            string url = urlTextBox.Text;

            JObject configJson = new JObject();
            configJson.Add(new JProperty("Url", url));

            string configJsonString = JsonConvert.SerializeObject(configJson);

            File.WriteAllText(config.Value<string>("appProjectDirectory") + config.Value<string>("appConfigFile"), configJsonString);

            statusTextBlock.Text += "App config created" + Environment.NewLine;

            StringBuilder sb = new StringBuilder();

            Process msbuild = new Process();
            msbuild.StartInfo.WorkingDirectory = config.Value<string>("appProjectDirectory");
            msbuild.StartInfo.FileName = config.Value<string>("msbuildDirectory") + "MSBuild.exe";
            // msbuild.StartInfo.UseShellExecute = true;
            msbuild.StartInfo.RedirectStandardOutput = true;

            msbuild.OutputDataReceived += (sender, args) => sb.AppendLine(args.Data);

            msbuild.StartInfo.Arguments = @"/t:SignAndroidPackage /p:Configuration=Release " + "\"" + config.Value<string>("appProjectDirectory") + config.Value<string>("appProjectFile") + "\"";
            msbuild.Start();

            statusTextBlock.Text += "Building" + Environment.NewLine;

            msbuild.BeginOutputReadLine();

            msbuild.WaitForExit();
            statusTextBlock.Text += sb.ToString();

            statusTextBlock.Text += "Build finished" + Environment.NewLine;

            buildButton.IsEnabled = true;
        }

        private void configButton_Click(object sender, RoutedEventArgs e)
        {
            ConfigWindow configWindow = new ConfigWindow(config);
            configWindow.ShowDialog();

            this.config = configWindow.Config;
            SaveConfig();
        }
    }
}
